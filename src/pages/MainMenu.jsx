import React from 'react';
import DashboardCard from '../components/dashboard/DashboardCard';
import { GridRow } from 'semantic-ui-react';

//importing sidemenu icons
import { ReactComponent as EtablissementIcon } from '../assets/icons/dashboard/school.svg';
import { ReactComponent as ElevesIcon } from '../assets/icons/dashboard/student_male.svg';
import { ReactComponent as PersonnelIcon } from '../assets/icons/dashboard/personnel.svg';
import { ReactComponent as NotesIcon } from '../assets/icons/dashboard/notes.svg';
import { ReactComponent as OutilsIcon } from '../assets/icons/dashboard/settings.svg';
import { ReactComponent as DisciplineIcon } from '../assets/icons/dashboard/discipline.svg';
import { ReactComponent as SurveillantIcon } from '../assets/icons/dashboard/surveillant.svg';
import { ReactComponent as PrincipalIcon } from '../assets/icons/dashboard/principal.svg';
import { ReactComponent as SeriesIcon } from '../assets/icons/dashboard/series.svg';
import { ReactComponent as EconomeIcon } from '../assets/icons/dashboard/econome.svg';
import { ReactComponent as NiveauIcon } from '../assets/icons/dashboard/niveau.svg';
import { ReactComponent as PrefetIcon } from '../assets/icons/dashboard/prefet.svg';
import { ReactComponent as CycleIcon } from '../assets/icons/dashboard/cycles.svg';
import { ReactComponent as ClasseIcon } from '../assets/icons/dashboard/classe.svg';

const MainMenu = () => {
    const menuItems = [
        {
            title: "Etablissement",
            icon: <EtablissementIcon />,
            amount: null
        },
        {
            title: "Surveillant",
            icon: <SurveillantIcon />,
            amount: 2,
            targetedPath: "/surveillant"
        },
        {
            title: "Discipline",
            icon: <DisciplineIcon />,
            amount: null,
            targetedPath: "/discipline"
        },
        {
            title: "Principal",
            icon: <PrincipalIcon />,
            amount: null,
            targetedPath: "/administration/anneesacademiques"
        },
        {
            title: "Notes",
            icon: <NotesIcon />,
            amount: null,
            targetedPath: "/notes"
        },
        {
            title: "Series",
            icon: <SeriesIcon />,
            amount: 21
        },
        {
            title: "Econome",
            icon: <EconomeIcon />,
            amount: 15,
            targetedPath: "/econome/inscription"
        },
        {
            title: "Personnel",
            icon: <PersonnelIcon />,
            amount: 21,
            targetedPath: "/personnel"
        },
        {
            title: "Niveau",
            icon: <NiveauIcon />,
            amount: null
        },
        {
            title: "Prefet d'études",
            icon: <PrefetIcon />,
            amount: null
        },
        {
            title: "Cycles",
            icon: <CycleIcon />,
            amount: 2,
            targetedPath: "/cycles"
        }
        ,
        {
            title: "Paramètres",
            icon: <OutilsIcon />,
            amount: null
        },
        {
            title: "Elèves",
            icon: <ElevesIcon />,
            amount: 453
        },
        {
            title: "Classes",
            icon: <ClasseIcon />,
            amount: 16,
            targetedPath: "/classes"
        }
    ];

    return (
        <GridRow stretched columns="three" className="dashboard-menu-cards" >
            {
                menuItems.map((menuItem, index) => {
                    return (
                        <DashboardCard key={index} targetedPath={menuItem.targetedPath} title={menuItem.title} icon={menuItem.icon} amount={menuItem.amount != null ? menuItem.amount : ""} />
                    );
                })
            }

        </GridRow>
    );
}

export default MainMenu;