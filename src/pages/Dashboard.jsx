import React, { Component } from 'react';
import { Grid, GridColumn, Card, GridRow, Dropdown } from 'semantic-ui-react';
import { Route, Link } from 'react-router-dom';
import '../styles/dashboard/dashboard.css';
import constants from '../utils/constants';

//importing component
import MainMenu from '../pages/MainMenu';
import Administration from '../pages/Administration';
import Econome from '../pages/Econome';
import Classes from '../pages/Classes';
import Notes from '../pages/Notes';
import Personnel from '../pages/Personnel';
import Surveillant from '../pages/Surveillant';
import Disciplines from '../pages/Disciplines';
import Cycles from '../pages/Cycles';



//importing sidemenu icons
import { ReactComponent as MenuIcon, ReactComponent } from '../assets/icons/menu.svg';
import { ReactComponent as DashBoardIcon } from '../assets/icons/dashboard/dashboard.svg';
import { ReactComponent as EtablissementIcon } from '../assets/icons/dashboard/school.svg';
import { ReactComponent as MatieresIcon } from '../assets/icons/dashboard/subject.svg';
import { ReactComponent as ElevesIcon } from '../assets/icons/dashboard/student_male.svg';
import { ReactComponent as PersonnelIcon } from '../assets/icons/dashboard/personnel.svg';
import { ReactComponent as PlanningIcon } from '../assets/icons/dashboard/planning.svg';
import { ReactComponent as NotesIcon } from '../assets/icons/dashboard/notes.svg';
import { ReactComponent as UserIcon } from '../assets/icons/dashboard/user.svg';
import { ReactComponent as OutilsIcon } from '../assets/icons/dashboard/settings.svg';
import { ReactComponent as DisciplineIcon } from '../assets/icons/dashboard/discipline.svg';

class Dashboard extends Component {

    state = {
        //items for the main menu of the dashboard

    }

    render() {
        return (
            <Grid className="app-container acrylic" >
                <Grid.Row columns='equal' textAlign="center" className="titleBar acrylic">
                    <GridColumn>
                        <div className="appTitle">
                            <div className="hamburgerMenu">
                                <MenuIcon />
                            </div>
                            <div className="appName">
                                SMaIS
                            </div>
                        </div>
                    </GridColumn>
                    <GridColumn width={13}>
                        SupAcademy
                    </GridColumn>
                    <GridColumn>
                        <Dropdown text='Langage'>
                            <Dropdown.Menu>
                                <Dropdown.Item text='Français' />
                                <Dropdown.Item text='Anglais' description='ctrl + o' />
                            </Dropdown.Menu>
                        </Dropdown>
                    </GridColumn>
                </Grid.Row>
                <Grid.Row columns='equal' className="workSpace">
                    <GridColumn className="sideMenu">
                        <Grid className="sideMenu-items-container acrylic">
                            <GridRow >
                                <div className="sidemenu-item">
                                    <div className="icon">
                                        <DashBoardIcon />
                                    </div>
                                    <div className="title">
                                        <Link to="/">
                                            Tableau de bord
                                        </Link>
                                    </div>
                                </div>
                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <EtablissementIcon />
                                </div>
                                <div className="title">
                                    Etablissement
                                </div>

                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <MatieresIcon />
                                </div>
                                <div className="title">
                                    Matière
                                </div>
                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <ElevesIcon />
                                </div>
                                <div className="title">
                                    Elèves
                                </div>
                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <PersonnelIcon />
                                </div>
                                <div className="title">
                                    Personnel
                                </div>
                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <PlanningIcon />
                                </div>
                                <div className="title">
                                    Planning
                                </div>
                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <DisciplineIcon />
                                </div>
                                <div className="title">
                                    Discipline
                                </div>
                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <NotesIcon />
                                </div>
                                <div className="title">
                                    Notes
                                </div>
                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <UserIcon />
                                </div>
                                <div className="title">
                                    Utilisateurs
                                </div>
                            </GridRow>
                            <GridRow>
                                <div className="icon">
                                    <OutilsIcon />
                                </div>
                                <div className="title">
                                    Outils
                                </div>
                            </GridRow>
                        </Grid>
                    </GridColumn>
                    <GridColumn width={14} className="dashboard-menu">
                        <Grid verticalAlign="middle" relaxed centered textAlign="center">
                            <Route path="/" exact>
                                <MainMenu menuItems={this.state.menuItems} />
                            </Route>
                            <Route path="/administration">
                                <Administration />
                            </Route>
                            <Route path="/econome">
                                <Econome />
                            </Route>
                            <Route path="/classes">
                                <Classes />
                            </Route>
                            <Route path="/notes">
                                <Notes />
                            </Route>
                            <Route path="/personnel">
                                <Personnel />
                            </Route>
                            <Route path="/surveillant">
                                <Surveillant />
                            </Route>
                            <Route path="/discipline">
                                <Disciplines />
                            </Route>
                            <Route path="/cycles">
                                <Cycles/>
                            </Route>
                        </Grid>
                    </GridColumn>
                </Grid.Row>
            </Grid>
        );
    }
}


export default Dashboard;
