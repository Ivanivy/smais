import React, { Component } from 'react'
import { GridRow, Grid, GridColumn, Search, Button, Modal, Image, Header, Form, Select, Label } from 'semantic-ui-react';
import { Link, Route } from 'react-router-dom';

//importing styles
import styles from '../styles/administration/administration.module.css';


//importing components
import AnneesAcademiques from '../components/administration/AnneesAcademiques';
import Classes from '../components/administration/Classes';
import Pension from '../components/administration/Pension';
import Apprenants from '../components/administration/Apprenants';

//importing assets
import logo from '../assets/icons/administration/logo.png';
import Personnel from '../components/administration/Personnel';



const Administration = () => {
    const [open, setOpen] = React.useState(false)
    const [tabNumber, setActiveTab] = React.useState(1);

    const principalOptions = [
        { key: '1', value: "M dongmo", text: "M Dongmo" },
        { key: '2', value: "M Albert", text: "M Albert" },
        { key: '3', value: "M Robert", text: "M Robert" },
    ]
    return (
        <GridRow stretched>
            <Grid className={`${styles.administrationContainer}`}>
                <GridRow columns="equal">
                    <GridColumn>
                        <div className={`${styles.logoContainer}`}>
                            <img src={logo} alt="logo" />
                        </div>
                    </GridColumn>
                    <GridColumn width="10" className={`${styles.infosContainer}`}>
                        <div style={{ display: "flex", flexDirection: "column" }}>
                            <div>
                                <div className="label">
                                    <strong>Nom:</strong>  Collège CEMM
                                </div>
                            </div>
                            <div>
                                <div className="label">
                                    <strong>Prix horaire:</strong>  7000
                                </div>
                            </div>
                        </div>
                    </GridColumn>
                    <GridColumn>
                        <div style={{ display: "flex", flexDirection: "column" }}>
                            <div>
                                <div className="label">
                                    <strong>Année Académique:</strong>  2019-2020
                                </div>
                            </div>
                            <div>
                                <div className="label">
                                    <strong>Nombre:</strong> <Label color="black">102</Label> apprenants
                                </div>
                            </div>
                        </div>
                    </GridColumn>
                </GridRow>
                <GridRow>
                    <Grid>
                        <GridRow className={styles.tabContainer}>
                            {/* //tabs */}
                            <div className={`${styles.tab}`}>
                                <button className={`tablinks ${tabNumber == 1 ? styles.selected : null}`} ><Link onClick={() => { setActiveTab(1) }} to="/administration/anneesacademiques">Années Académiques</Link></button>
                                <button className={`tablinks ${tabNumber == 2 ? styles.selected : null}`} ><Link onClick={() => { setActiveTab(2) }} to="/administration/classes" >Classes</Link></button>
                                <button className={`tablinks ${tabNumber == 3 ? styles.selected : null}`} ><Link onClick={() => { setActiveTab(3) }} to="/administration/apprenants" >Apprenants</Link></button>
                                <button className={`tablinks ${tabNumber == 4 ? styles.selected : null}`} ><Link onClick={() => { setActiveTab(4) }} to="/administration/personnel" >Personnel</Link></button>
                                <button className={`tablinks ${tabNumber == 5 ? styles.selected : null}`} ><Link onClick={() => { setActiveTab(5) }} to="/administration/pension" >Pension</Link></button>
                            </div>
                        </GridRow>
                        <GridRow className={styles.searchContainer}>
                            <Search />
                            <Modal
                                onClose={() => setOpen(false)}
                                onOpen={() => setOpen(true)}
                                open={open}
                                trigger={<Button>Ajouter</Button>}
                            >
                                <Route path="/administration/anneesacademiques" >
                                    <Modal.Header>Ajouter une année Académique</Modal.Header>
                                </Route>
                                <Route path="/administration/classes" exact>
                                    <Modal.Header>Ajouter une classe</Modal.Header>
                                </Route>
                                <Route path="/administration/pension" exact>
                                    <Modal.Header>Ajouter une pension</Modal.Header>
                                </Route>
                                <Route path="/administration/personnel" exact>
                                    <Modal.Header>Ajouter un personnel</Modal.Header>
                                </Route>
                                <Route path="/administration/apprenants" exact>
                                    <Modal.Header>Ajouter un apprenant</Modal.Header>
                                </Route>


                                <Modal.Content image>
                                    <Modal.Description>
                                        {/* ajouter une année académique début */}
                                        <Route path="/administration/anneesacademiques" exact>
                                            <Form>
                                                <Form.Field>
                                                    <label>Nom Cycle</label>
                                                    <input placeholder='Nom cycle' />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Section</label>
                                                    <input placeholder='Section' />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Année académique</label>
                                                    <input placeholder='Section' />
                                                </Form.Field>
                                            </Form>
                                        </Route>
                                        <Route path="/administration/anneesacademiques/addannestep2">
                                            <Form>
                                                <Form.Field>
                                                    <label>Ajout principal</label>
                                                    <Select placeholder='Selectionner le principal' options={principalOptions} />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Ajout des surveillants</label>
                                                    <Select placeholder='Selectionner le surveillant' options={principalOptions} />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Ajout des prefets d'études</label>
                                                    <Select placeholder='Selectionner le préfet d étude' options={principalOptions} />
                                                </Form.Field>
                                            </Form>
                                        </Route>
                                        {/* ajouter une année académique fin */}

                                        {/* ajouter une  classe debut */}
                                        <Route path="/administration/classes">
                                            <Form>
                                                <Form.Field>
                                                    <label>Nom classe:</label>
                                                    <input placeholder='Nom de la classe' />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Pension</label>
                                                    <input placeholder='Pension' />
                                                </Form.Field>
                                            </Form>
                                        </Route>

                                        {/* ajouter une classe fin */}

                                        {/* ajouter une  pension debut */}
                                        <Route path="/administration/pension">
                                            <Form>
                                                <Form.Field>
                                                    <label>Classe:</label>
                                                    <Select placeholder='Selectioner une classe' options={principalOptions} />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Inscription</label>
                                                    <input placeholder="frais d'inscription" />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>1ère tranche</label>
                                                    <input placeholder="Montant de la première tranche" />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>2ème tranche</label>
                                                    <input placeholder="Montant de la deuxième tranche" />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Total: 65000</label>
                                                </Form.Field>
                                            </Form>
                                        </Route>
                                        {/* ajouter une pension fin */}

                                        {/* ajotuer un personnel début*/}
                                        <Route path="/administration/personnel">
                                            <Form>
                                                <Form.Field>
                                                    <label>Nom:</label>
                                                    <input placeholder="Entre le nom" />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Grade:</label>
                                                    <Select placeholder='Entrer le grade' options={principalOptions} />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Statut:</label>
                                                    <Select placeholder='statut du personnel' options={principalOptions} />
                                                </Form.Field>
                                            </Form>
                                        </Route>
                                        {/* ajouter un personnel fin */}
                                        {/* ajotuer un apprenant début*/}
                                        <Route path="/administration/apprenants">
                                            <Form>
                                                <Form.Field>
                                                    <label>Nom:</label>
                                                    <input placeholder="Entre le nom" />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Classe:</label>
                                                    <Select placeholder='Choisissez la classe' options={principalOptions} />
                                                </Form.Field>
                                                <Form.Field>
                                                    <label>Année Academique:</label>
                                                    <Select placeholder='Année académique' options={principalOptions} />
                                                </Form.Field>
                                            </Form>
                                        </Route>
                                        {/* ajouter un apprenant fin */}


                                    </Modal.Description>
                                </Modal.Content>
                                <Modal.Actions>
                                    <Button color='black' onClick={() => setOpen(false)}>
                                        Annuler
                                    </Button>
                                    <Button
                                        content={
                                            <div>
                                                <Route path="/administration/anneesacademiques">
                                                    <Link to="/administration/anneesacademiques/addannestep2">Suivant</Link>
                                                </Route>
                                                <Route path="/administration/classes">
                                                    <Link onClick={() => { setOpen(false) }} to="/administration/classes">Terminer</Link>
                                                </Route>
                                                <Route path="/administration/pension">
                                                    <Link onClick={() => { setOpen(false) }} to="/administration/pension">Terminer</Link>
                                                </Route>
                                                <Route path="/administration/personnel">
                                                    <Link onClick={() => { setOpen(false) }} to="/administration/personnel">Terminer</Link>
                                                </Route>
                                                <Route path="/administration/Apprenant">
                                                    <Link onClick={() => { setOpen(false) }} to="/administration/apprenants">Terminer</Link>
                                                </Route>
                                            </div>
                                        }
                                        labelPosition='right'
                                        icon='checkmark'

                                    // onClick={() => setOpen(false)}
                                    />
                                </Modal.Actions>
                            </Modal>
                        </GridRow>
                        <GridRow>
                            <Route path="/administration/anneesacademiques" >
                                <AnneesAcademiques />
                            </Route>
                            <Route path="/administration/classes" exact>
                                <Classes />
                            </Route>
                            <Route path="/administration/pension" exact>
                                <Pension />
                            </Route>
                            <Route path="/administration/personnel" exact>
                                <Personnel />
                            </Route>
                            <Route path="/administration/apprenants" exact>
                                <Apprenants />
                            </Route>
                        </GridRow>
                    </Grid>
                </GridRow>
            </Grid>
        </GridRow>
    );
}

export default Administration;