import React, { Component } from 'react'
import { Grid, GridRow, Form, Select, Search, Table, TableRow, TableHeader, TableCell, TableBody, Modal, Button, Input, ModalActions } from 'semantic-ui-react';
import styles from '../styles/disciplines/discipline.module.css';

const Disciplines = ({ }) => {
    const principalOptions = [
        { key: '1', value: "3ème", text: "3ème" },
        { key: '2', value: "4ème", text: "4ème" },
        { key: '3', value: "5ème", text: "5ème" },
    ]
    return (
        <Grid>
            <GridRow>
                <div style={{ width: "100%" }}>
                    <h2>Liste des matières</h2>
                </div>
            </GridRow>
            <GridRow>
                <div style={{ width: "100%", display: "flex", justifyContent: "flex-end", alignItems: "center", marginTop: "1rem" }}>
                    <Search placeholder="rechercher" />
                    <div style={{ margin: "1rem" }}></div>
                    <Modal trigger={<Button>Ajouter</Button>}>
                        <Form>
                            <div style={{ marginTop: "1rem", marginBottom: "1rem" }}>
                                <Form.Field >
                                    <label>Nom Matières: </label>
                                    <Input></Input>
                                </Form.Field>
                                <Form.Field >
                                    <label>Coefficient: </label>
                                    <Input></Input>
                                </Form.Field>
                                <Form.Field >
                                    <label>Nom Enseignant: </label>
                                    <Input></Input>
                                </Form.Field>

                            </div>
                        </Form>
                        <ModalActions>
                            <Button>Ok</Button>
                        </ModalActions>
                    </Modal>
                </div>
            </GridRow>
            <GridRow>
                <Form>
                    <Form.Field inline>
                        <label>Classe: </label>
                        <Select placeholder='Choisir la classe' options={principalOptions} />
                    </Form.Field>
                </Form>
            </GridRow>
            <GridRow>
                <Table>
                    <TableHeader>
                        <TableRow>
                            <TableCell>
                                <strong>Nom Matières</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Coef</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Noms Enseignant</strong>
                            </TableCell>
                            <TableCell>
                                <strong> </strong>
                            </TableCell>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                Mathématiques
                            </TableCell>
                            <TableCell>
                                2
                            </TableCell>
                            <TableCell>
                                Tchoukeu Micel
                            </TableCell>
                            <TableCell>
                                <Button>Modifier</Button>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                Mathématiques
                            </TableCell>
                            <TableCell>
                                2
                            </TableCell>
                            <TableCell>
                                Tchoukeu Micel
                            </TableCell>
                            <TableCell>
                                <Button>Modifier</Button>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                Mathématiques
                            </TableCell>
                            <TableCell>
                                2
                            </TableCell>
                            <TableCell>
                                Tchoukeu Micel
                            </TableCell>
                            <TableCell>
                                <Button>Modifier</Button>
                            </TableCell>
                        </TableRow>

                    </TableBody>
                </Table>
            </GridRow>
        </Grid>
    )
}

export default Disciplines;