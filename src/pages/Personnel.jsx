import React from 'react'
import { Grid, GridColumn, Table, TableHeader, TableCell, TableBody, TableRow, Modal, Label, Search, ModalActions, Button, GridRow } from 'semantic-ui-react';
import styles from '../styles/personnel/personnel.module.css';
import logo from '../assets/icons/administration/logo.png';

const Personnel = ({ }) => {
    return (
        <Grid>
            <GridRow>
                <div className={styles.inlineContainer}>
                    <div>
                        <div><h3>Sup Academy</h3></div>
                        <div className={styles.logoContainer}><img src={logo} alt="logo" /></div>
                    </div>
                    <div>
                        <div><Button>Imprimer</Button></div>
                        <div><Search /></div>
                    </div>
                </div>
            </GridRow>
            <GridRow >

            </GridRow>
            <GridRow>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.Cell>
                                <strong>
                                    Noms
                                </strong>
                            </Table.Cell>
                            <Table.Cell>
                                <strong>
                                    Grades
                                </strong>
                            </Table.Cell>
                            <Table.Cell>
                                <strong>
                                    Statut
                                </strong>
                            </Table.Cell>
                            <Table.Cell>
                                <strong>
                                    Photo
                                </strong>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Header>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                Fotso Brice
                            </TableCell>
                            <TableCell>
                                PREFET
                            </TableCell>
                            <TableCell>
                                Permanant
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.profileContainer}`}>
                                    <img src={logo} alt="logo" />
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                Fotso Brice
                            </TableCell>
                            <TableCell>
                                PREFET
                            </TableCell>
                            <TableCell>
                                Permanant
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.profileContainer}`}>
                                    <img src={logo} alt="logo" />
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                Fotso Brice
                            </TableCell>
                            <TableCell>
                                PREFET
                            </TableCell>
                            <TableCell>
                                Permanant
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.profileContainer}`}>
                                    <img src={logo} alt="logo" />
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                Fotso Brice
                            </TableCell>
                            <TableCell>
                                PREFET
                            </TableCell>
                            <TableCell>
                                Permanant
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.profileContainer}`}>
                                    <img src={logo} alt="logo" />
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                Fotso Brice
                            </TableCell>
                            <TableCell>
                                PREFET
                            </TableCell>
                            <TableCell>
                                Permanant
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.profileContainer}`}>
                                    <img src={logo} alt="logo" />
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                Fotso Brice
                            </TableCell>
                            <TableCell>
                                PREFET
                            </TableCell>
                            <TableCell>
                                Permanant
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.profileContainer}`}>
                                    <img src={logo} alt="logo" />
                                </div>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </GridRow>
        </Grid>
    );
}

export default Personnel;