import React, { Component } from 'react'
import { GridRow, Grid, GridColumn, Form } from 'semantic-ui-react';
import { Link, Route } from 'react-router-dom';
import styles from '../styles/econome/econome.module.css';



//importing componetnts
import AjouterApprenant from '../components/econome/AjouterApprenant';
import Pension from '../components/econome/Pension';
import Apprenant from '../components/econome/Apprenant';
import EntreesSorties from '../components/econome/EntreesSorties';
import Salaire from '../components/econome/Salaire';
import EtatsFinanciers from '../components/econome/EtatsFinanciers';


const Econome = () => {
    return (
        <Grid>
            <GridRow stretched>
                <GridRow className={'tabContainer'} stretched>
                    {/* //tabs */}
                    <div className={`tab`}>
                        <button className={`tablinks selected`} ><Link to="/anneesacademiques">Econome</Link></button>
                    </div>
                </GridRow>
                <GridRow stretched className={`full-width`}>
                    <Grid stretched columns="equal" className={`full-width ${styles.workspace}`}>
                        <GridColumn className={`full-height ${styles.economeSideMenu}`}>
                            <Grid>
                                <GridRow>
                                    <strong><Link to="/econome/inscription">Inscription</Link></strong>
                                </GridRow>
                                <GridRow>
                                    <strong><Link to="/econome/pension">Pension</Link></strong>
                                </GridRow>
                                <GridRow>
                                    <strong><Link to="/econome/salaire">Salaire</Link></strong>
                                </GridRow>
                                <GridRow>
                                    <strong><Link to="/econome/entreessorties">Entrées/Sortie</Link></strong>
                                </GridRow>
                                <GridRow>
                                    <strong><Link to="/econome/etatsfinanciers">Etat des Finances</Link></strong>
                                </GridRow>
                                <GridRow>
                                    <strong><Link>Ajout</Link></strong>
                                </GridRow>
                                <GridRow>
                                    <strong><Link to="/econome/apprenant">Apprenant</Link></strong>
                                </GridRow>
                            </Grid>
                        </GridColumn>
                        <GridColumn width="14" className={`${styles.economeWorkspace}`}>
                            {/* Selectionnez une option */}
                            <Route path="/econome/inscription">
                                <AjouterApprenant styles={styles} />
                            </Route>
                            <Route path="/econome/pension">
                                <Pension styles={styles} />
                            </Route>
                            <Route path="/econome/apprenant">
                                <Apprenant styles={styles} />
                            </Route>
                            <Route path="/econome/entreessorties">
                                <EntreesSorties styles={styles} />
                            </Route>
                            <Route path="/econome/salaire">
                                <Salaire styles={styles} />
                            </Route>
                            <Route path="/econome/etatsfinanciers">
                                <EtatsFinanciers styles={styles} />
                            </Route>
                        </GridColumn>
                    </Grid>
                </GridRow>
            </GridRow>
        </Grid>
    );
}


export default Econome;