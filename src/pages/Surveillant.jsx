import React, { Component } from 'react'
import { Grid, GridRow, GridColumn, Select } from 'semantic-ui-react';
import styles from '../styles/surveillant/surveillant.module.css';
import { Route, withRouter } from 'react-router-dom';
import { ReactComponent as ElevesIcon } from '../assets/icons/dashboard/student_male.svg';
import { ReactComponent as PersonnelIcon } from '../assets/icons/dashboard/personnel.svg';
import Personnel from './Personnel';


//importing components
import PresencesClasses from '../components/surveillant/PresencesClasse';
import PresenceEnseignant from '../components/surveillant/PresencesEnseignant';
import PresencesEnseignant from '../components/surveillant/PresencesEnseignant';


const Surveillant = ({ history }) => {
    return (
        <Grid>
            <Route path="/surveillant" exact>
                <GridRow>
                    <div className={styles.title}>
                        <h2>Présences</h2>
                    </div>
                </GridRow>
                <GridRow>
                    <GridColumn>
                        <div className={styles.presenceOptions}>
                            <div className={styles.presenceCard} onClick={() => { history.push("/surveillant/presenceeleves") }}>
                                <div>
                                    <div>
                                        <ElevesIcon />
                                    </div>
                                    <h3>
                                        Elèves
                                </h3>
                                </div>
                                <div>

                                </div>
                            </div>
                            <div className={styles.presenceCard} onClick={() => { history.push("/surveillant/presencesenseignant") }}>
                                <div>
                                    <div>
                                        <PersonnelIcon />
                                    </div>
                                    <h3>
                                        Enseignants
                                </h3>
                                </div>
                                <div>

                                </div>
                            </div>
                        </div>
                    </GridColumn>
                </GridRow>
            </Route>
            <Route path="/surveillant/presenceeleves">
                <PresencesClasses styles={styles} />
            </Route>
            <Route path="/surveillant/presencesenseignant">
                <PresencesEnseignant styles={styles} />
            </Route>
        </Grid>
    )
}
export default withRouter(Surveillant);