import React from 'react'
import { GridColumn, Table, TableHeader, TableCell, TableBody, TableRow, Modal, Label, Search, ModalActions, Button } from 'semantic-ui-react';

const Notes = ({ }) => {
    return (
        <GridColumn>
            <Table>
                <TableHeader>
                    <TableRow >
                        <TableCell>
                            <center><h3><strong>NOTES</strong></h3></center>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>
                            Classes
                        </TableCell>
                        <TableCell>
                            Sequences
                        </TableCell>
                        <TableCell>
                            Matières
                        </TableCell>
                    </TableRow>
                </TableHeader>
                <TableBody>
                    <TableRow>
                        <TableCell>
                            <strong>3ème</strong>
                        </TableCell>
                        <TableCell>
                            <strong>2 ème sequence</strong>
                        </TableCell>
                        <TableCell>
                            <strong>Mathématiques</strong>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>
                            <strong>3ème</strong>
                        </TableCell>
                        <TableCell>
                            <strong>2 ème sequence</strong>
                        </TableCell>
                        <TableCell>
                            <strong>Mathématiques</strong>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>
                            <strong>3ème</strong>
                        </TableCell>
                        <TableCell>
                            <strong>2 ème sequence</strong>
                        </TableCell>
                        <TableCell>
                            <strong>Mathématiques</strong>
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>
                            <strong>3ème</strong>
                        </TableCell>
                        <TableCell>
                            <strong>2 ème sequence</strong>
                        </TableCell>
                        <TableCell>
                            <strong>Mathématiques</strong>
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
            <Modal open={false}>
                <Table celled>
                    <TableHeader>
                        <TableRow>
                            <TableCell colSpan={4}>
                                <div style={{ display: "flex", alignContent: "center", justifyContent: "space-around" }}>
                                    <div>
                                        Classe: <Label color={"black"}>1ère</Label>
                                    </div>
                                    <div>
                                        Evalutation: <Label color={"black"}>03</Label>
                                    </div>
                                    <div>
                                        Matière: <Label color={"black"}>Physique</Label>
                                    </div>
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={4}>
                                <div style={{ display: "flex", justifyContent: "flex-end" }}>
                                    <Search />
                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <strong>
                                    Matricule
                                </strong>
                            </TableCell>
                            <TableCell>
                                <strong>
                                    Noms et prénoms
                                </strong>
                            </TableCell>
                            <TableCell>
                                <strong>
                                    Notes
                                </strong>
                            </TableCell>
                            <TableCell>
                                <strong>
                                    Appréciation
                                </strong>
                            </TableCell>
                        </TableRow>
                    </TableHeader>
                    <TableBody >
                        <TableRow>
                            <TableCell>
                                CEM01
                            </TableCell>
                            <TableCell>
                                Enama Etoga
                            </TableCell>
                            <TableCell>
                                15
                            </TableCell>
                            <TableCell>
                                ECA
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                CEM01
                            </TableCell>
                            <TableCell>
                                Enama Etoga
                            </TableCell>
                            <TableCell>
                                15
                            </TableCell>
                            <TableCell>
                                ECA
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                CEM01
                            </TableCell>
                            <TableCell>
                                Enama Etoga
                            </TableCell>
                            <TableCell>
                                15
                            </TableCell>
                            <TableCell>
                                ECA
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                CEM01
                            </TableCell>
                            <TableCell>
                                Enama Etoga
                            </TableCell>
                            <TableCell>
                                15
                            </TableCell>
                            <TableCell>
                                ECA
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </Modal>
            <Modal open={false}>
                <div>
                    <Table celled={false} >
                        <TableBody>
                            <TableRow>
                                <TableCell>Matiere </TableCell>
                                <TableCell>Mathématiques</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Nombre de devoirs </TableCell>
                                <TableCell><Label color={"black"}>n </Label></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Notes 1er devoir </TableCell>
                                <TableCell>  <Label color={"black"}><strong >19</strong> / 20</Label></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>notes 2eme devoir </TableCell>
                                <TableCell>  <Label color={"black"}><strong >19</strong> / 20</Label></TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Notes nieme devoir </TableCell>
                                <TableCell>  <Label color={"black"}><strong >19</strong> / 20</Label></TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </div>
                <ModalActions>
                    <Button>Ok</Button>
                </ModalActions>
            </Modal>
        </GridColumn >
    )
}

export default Notes;