import React, { Component } from 'react'
import { Grid, GridRow, Input, Search, Button, List, Table, TableHeader, TableCell, TableBody, Modal, Form, ModalActions } from 'semantic-ui-react';

const Cycles = ({ }) => {
    return (
        <Grid>
            <GridRow>
                <div style={{ width: "100%", marginTop: "1rem" }}>
                    <h2>Cycles</h2>
                </div>
            </GridRow>
            <GridRow>
                <div style={{ width: "100%", display: "flex", justifyContent: "flex-end" }}>
                    <Search />
                    <div style={{ margin: "1rem" }}></div>
                    <Modal trigger={<Button>+ Ajouter Cycle</Button>}>
                        <Form>
                            <Form.Field>
                                <label>Année académique:</label>
                                <Input></Input>
                            </Form.Field>
                            <Form.Field>
                                <label>Nom du cycle:</label>
                                <Input></Input>
                            </Form.Field>
                        </Form>
                        <ModalActions>
                            <Button>Ok</Button>
                        </ModalActions>
                    </Modal>
                </div>
            </GridRow>
            <GridRow>
                <Table celled sortable textAlign={"center"}>
                    <TableHeader>
                        <strong>
                            <TableCell>Liste des cycles</TableCell>
                        </strong>
                    </TableHeader>
                    <TableBody>
                        <TableHeader>
                            <strong>
                                <TableCell>1er Cycle</TableCell>
                            </strong>
                        </TableHeader>
                        <TableHeader>
                            <strong>
                                <TableCell>2e Cycle</TableCell>
                            </strong>
                        </TableHeader>
                        <TableHeader>
                            <strong>
                                <TableCell>3e Cycle</TableCell>
                            </strong>
                        </TableHeader>
                    </TableBody>
                </Table>
            </GridRow>
        </Grid>
    );

}

export default Cycles;