import React, { Component } from 'react'
import { GridColumn, GridRow, Search, Grid, Table, TableBody, Modal, Button, Form, Menu, Icon, PlaceholderImage, Image, ModalHeader, Select, Radio } from 'semantic-ui-react';
import { Link, Route } from 'react-router-dom';
import styles from '../styles/classes/classes.module.css';


//importing components

import ListeEleves from '../components/classes/ListeEleves';
import Emploitemps from '../components/classes/Emploitemps';
import Matieres from '../components/classes/Matieres';

class Classes extends Component {
    state = {
        open: true,
        tabNumber: 1,
        openEleve: false,
    }

    setActiveTab(tabNumber) {
        this.setState({ tabNumber });
    }

    render() {
        const principalOptions = [
            { key: '1', value: "3ème", text: "3ème" },
            { key: '2', value: "4ème", text: "4ème" },
            { key: '3', value: "5ème", text: "5ème" },
        ]

        return (
            <Grid>
                <GridRow>
                    <div style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                        <Search />
                        <Modal
                            trigger={<Button>+ Ajouter</Button>}
                        >
                            <ModalHeader>
                                <h1 style={{ textAlign: "center" }}>Ajouter classe</h1>
                            </ModalHeader>
                            <Form>
                                <Form.Field >
                                    <label>cycle:</label>
                                    <Select placeholder='Choisissez le cycle' options={principalOptions} />
                                </Form.Field>
                                <Form.Field >
                                    <label>Année académique:</label>
                                    <Select placeholder='Année académique' options={principalOptions} />
                                </Form.Field>
                                <Form.Field >
                                    <label>Inscription:</label>
                                    <input placeholder="frais d'inscription" />
                                </Form.Field>
                                <Form.Field >
                                    <label>première tranche:</label>
                                    <input placeholder="Montant de la première tranche" />
                                </Form.Field>
                                <Form.Field >
                                    <label>Deuxième tranche:</label>
                                    <input placeholder="Montant de la première tranche" />
                                </Form.Field>
                                <Form.Field >
                                    <label>Troisième tranche:</label>
                                    <input placeholder="Montant de la troisième tranche" />
                                </Form.Field>
                            </Form>
                            <Modal.Actions>
                                <Button>Ok</Button>
                            </Modal.Actions>
                        </Modal>

                    </div>
                </GridRow>
                <GridRow >
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell width="8">Classe </Table.HeaderCell>
                                <Table.HeaderCell>Effectif</Table.HeaderCell>
                                <Table.HeaderCell>Nombre de matières</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>6<sup>eme</sup> A</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>75</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>6<sup>eme</sup> B</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>56</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>5<sup>eme</sup> A</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>48</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>6<sup>eme</sup> A</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>75</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>6<sup>eme</sup> B</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>56</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>5<sup>eme</sup> A</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>48</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>6<sup>eme</sup> A</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>75</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>6<sup>eme</sup> B</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>56</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell textAlign="left"><strong><Link>5<sup>eme</sup> A</Link></strong></Table.Cell>
                                <Table.Cell width="10" ><Link>48</Link></Table.Cell>
                                <Table.Cell >09</Table.Cell>
                            </Table.Row>

                        </TableBody>
                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='7'>
                                    <Menu floated='right' pagination>
                                        <Menu.Item as='a' icon>
                                            <Icon name='chevron left' />
                                        </Menu.Item>
                                        <Menu.Item as='a'>1</Menu.Item>
                                        <Menu.Item as='a'>2</Menu.Item>
                                        <Menu.Item as='a'>3</Menu.Item>
                                        <Menu.Item as='a'>4</Menu.Item>
                                        <Menu.Item as='a' icon>
                                            <Icon name='chevron right' />
                                        </Menu.Item>
                                    </Menu>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>

                    <Modal open={this.state.open} closeOnDimmerClick={"true"} closeOnDocumentClick={"true"}>
                        <Modal.Header>
                            <div className={styles.classeHeader}>
                                <GridRow className={styles.tabContainer}>
                                    {/* //tabs */}
                                    <div className={`${styles.tab}`}>
                                        <button className={`tablinks ${this.state.tabNumber == 1 ? styles.selected : null}`} ><Link onClick={() => { this.setActiveTab(1) }} to="/classes/eleves" >Liste élèves</Link></button>
                                        <button className={`tablinks ${this.state.tabNumber == 2 ? styles.selected : null}`} ><Link onClick={() => { this.setActiveTab(2) }} to="/classes/matieres">Matières</Link></button>
                                        <button className={`tablinks ${this.state.tabNumber == 3 ? styles.selected : null}`} ><Link onClick={() => { this.setActiveTab(3) }} to="/classes/emploitemps">Emploi du temps</Link></button>
                                    </div>
                                </GridRow>
                            </div>
                        </Modal.Header>

                        {/* différents pages inside the main page */}
                        <ListeEleves styles={styles} />
                        <Emploitemps styles={styles} />
                        <Matieres styles={styles} />

                    </Modal>

                    <Modal open={this.state.openEleve} closeOnDimmerClick={"true"} closeOnDocumentClick={"true"}>
                        <Modal.Header>
                            <div className={styles.classeHeader}>
                                <GridRow >
                                    <div style={{ width: "100%", display: "flex", justifyContent: "space-around", margin: "1rem" }}>
                                        <div>
                                            <h3>Nom Elève: TSOUNGI SIMON PIERRE</h3>
                                        </div>
                                        <div>
                                            <h3>Sexe : Masculin</h3>
                                        </div>
                                    </div>
                                </GridRow>
                            </div>
                        </Modal.Header>
                        <div style={{ width: "100%", display: "flex", justifyContent: "flex-end", margin: "1rem" }}>
                            <Search />
                        </div>
                        <div>
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell width="4">Liste matières </Table.HeaderCell>
                                        <Table.HeaderCell>Séquence </Table.HeaderCell>
                                        <Table.HeaderCell>Coef </Table.HeaderCell>
                                        <Table.HeaderCell>Note </Table.HeaderCell>
                                        <Table.HeaderCell>Appréciation </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <TableBody>
                                    <Table.Row>
                                        <Table.Cell textAlign="left"><Link>Mathématiques</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>1ère sequence</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>4</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>20</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>Execellent</Link></Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell textAlign="left"><Link>Mathématiques</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>1ère sequence</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>4</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>20</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>Execellent</Link></Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell textAlign="left"><Link>Mathématiques</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>1ère sequence</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>4</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>20</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>Execellent</Link></Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell textAlign="left"><Link>Mathématiques</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>1ère sequence</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>4</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>20</Link></Table.Cell>
                                        <Table.Cell textAlign="left"><Link>Execellent</Link></Table.Cell>
                                    </Table.Row>
                                </TableBody>
                                <Table.Footer>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='7'>
                                            <Menu floated='right' pagination>
                                                <Menu.Item as='a' icon>
                                                    <Icon name='chevron left' />
                                                </Menu.Item>
                                                <Menu.Item as='a'>1</Menu.Item>
                                                <Menu.Item as='a'>2</Menu.Item>
                                                <Menu.Item as='a'>3</Menu.Item>
                                                <Menu.Item as='a'>4</Menu.Item>
                                                <Menu.Item as='a' icon>
                                                    <Icon name='chevron right' />
                                                </Menu.Item>
                                            </Menu>
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Footer>
                            </Table>
                        </div>
                    </Modal>
                </GridRow>
            </Grid >
        );
    }
}

export default Classes;