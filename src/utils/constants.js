const constants = {
    colors: {
        //defining styles for colors
        colorPrimary: "#0078d4",
        colorSecondary: "#00adef",
        colorSecondaryDarker: "#423f3e",
        colorBackground: "#faf9f8",
        colorComponent: "#ffffff",
    },
    microsoftColors: [
        "#a4373a",
        "#0078d4",
        "#217346",
        "#d83b01",
        "#0078d4",
        "#7719aa",
        "#31752f",
        "#742774",
        "#b7472a",
        "#077568",
        "#2b579a" 
    ]
    //defining global styles for fonts
}





export default constants; 