import React, { Component } from 'react'
import { Route, Link } from 'react-router-dom';
import { Search, Table, GridRow, TableBody, Menu, Icon } from 'semantic-ui-react';

const Emploitemps = ({ styles }) => {
    return (
        <Route path="/classes/emploitemps">
            <div className={styles.classeHeader}>
                <GridRow >
                    <div style={{ width: "100%", display: "flex", margin: "1rem" }}>
                        <div>
                            <h3>Nom Classe: 4 <sup>ème</sup>A</h3>
                        </div>
                    </div>
                </GridRow>
            </div>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width="4">Horaire </Table.HeaderCell>
                        <Table.HeaderCell>Lundi </Table.HeaderCell>
                        <Table.HeaderCell>Mardi </Table.HeaderCell>
                        <Table.HeaderCell>Mercredi </Table.HeaderCell>
                        <Table.HeaderCell>Jeudi </Table.HeaderCell>
                        <Table.HeaderCell>Vendredi </Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <TableBody>
                    <Table.Row>
                        <Table.Cell textAlign="left"><Link>7h30-9h30</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Mathématiques</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Français</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Philosophie</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Anglais</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Physique</Link></Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell textAlign="left"><Link>7h30-9h30</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Mathématiques</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Français</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Philosophie</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Anglais</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Physique</Link></Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell textAlign="left"><Link>7h30-9h30</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Mathématiques</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Français</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Philosophie</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Anglais</Link></Table.Cell>
                        <Table.Cell textAlign="left"><Link>Physique</Link></Table.Cell>
                    </Table.Row>

                </TableBody>
                <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell colSpan='7'>
                            <Menu floated='right' pagination>
                                <Menu.Item as='a' icon>
                                    <Icon name='chevron left' />
                                </Menu.Item>
                                <Menu.Item as='a'>1</Menu.Item>
                                <Menu.Item as='a'>2</Menu.Item>
                                <Menu.Item as='a'>3</Menu.Item>
                                <Menu.Item as='a'>4</Menu.Item>
                                <Menu.Item as='a' icon>
                                    <Icon name='chevron right' />
                                </Menu.Item>
                            </Menu>
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>
            </Table>
        </Route>
    );
}

export default Emploitemps;