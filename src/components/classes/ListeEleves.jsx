import React, { Component, useState } from 'react';
import { Route, Link } from 'react-router-dom';
import { Search, Table, TableBody, Menu, Icon, Modal, GridRow } from 'semantic-ui-react';

const ListeEleves = ({ styles }) => {
    const [eleveOpen, seteleveOpen] = useState(true)
    return (
        <div>
            <Route path="/classes/eleves" exact>
                <div>
                    <div style={{ width: "100%", display: "flex", justifyContent: "space-around", margin: "1rem" }}>
                        <div>
                            <h3>Nom classe: 3<sup>ème</sup></h3>
                        </div>
                        <div>
                            <h3>Effectif : 47</h3>
                        </div>
                    </div>
                    <div style={{ width: "100%", display: "flex", justifyContent: "flex-end", margin: "1rem" }}>
                        <Search />
                    </div>
                    <div>
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell width="4">Nom </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <TableBody>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>TSOUNGI SIMON PIERRE</Link></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>TSOUNGI SIMON PIERRE</Link></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>TSOUNGI SIMON PIERRE</Link></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>TSOUNGI SIMON PIERRE</Link></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>TSOUNGI SIMON PIERRE</Link></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>TSOUNGI SIMON PIERRE</Link></Table.Cell>
                                </Table.Row>
                            </TableBody>
                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='7'>
                                        <Menu floated='right' pagination>
                                            <Menu.Item as='a' icon>
                                                <Icon name='chevron left' />
                                            </Menu.Item>
                                            <Menu.Item as='a'>1</Menu.Item>
                                            <Menu.Item as='a'>2</Menu.Item>
                                            <Menu.Item as='a'>3</Menu.Item>
                                            <Menu.Item as='a'>4</Menu.Item>
                                            <Menu.Item as='a' icon>
                                                <Icon name='chevron right' />
                                            </Menu.Item>
                                        </Menu>
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>
                    </div>
                </div>
            </Route>
        </div>
    );
}
export default ListeEleves;