import React from 'react'
import { Route, Link } from 'react-router-dom';
import { Table, TableBody, Modal, ModalHeader, Button, Label, ModalActions } from 'semantic-ui-react';
const Matieres = ({ }) => {
    return (
        <Route path="/classes/matieres">
            <Modal open={true}>
                <div style={{ width: "100%", display: "flex", justifyContent: "space-around", margin: "1rem" }}>
                    <div>
                        <h3>
                            Choix du mode:
                        </h3>
                        <Modal trigger={<Button>pourcentage</Button>}>
                            <ModalHeader>
                                <div style={{ display: "flex" }}>
                                    <div>
                                        Nombre de devoirs:
                                    </div>
                                    <Label color="black">02</Label>
                                </div>
                            </ModalHeader>
                            <div style={{ margin: "1rem" }}>
                                <div style={{ marginTop: "1rem" }}>
                                    1<sup>er</sup> devoir: <Label color="black">30 %</Label>
                                </div>
                                <div style={{ marginTop: "1rem" }}>
                                    2<sup>e</sup> devoir: <Label color="black">30 %</Label>
                                </div>
                            </div>
                            <ModalActions>
                                <Button>Ok</Button>
                            </ModalActions>
                        </Modal>
                        <Modal trigger={<Button>Empirique</Button>}>
                            <div>
                                <center> <h3>Nombre de devoirs:</h3> <Label color="black"><h3>04</h3></Label> </center>
                            </div>
                            <ModalActions>
                                <Button>Ok</Button>
                            </ModalActions>
                        </Modal>

                    </div>
                </div>
            </Modal>
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width="8">Liste matières </Table.HeaderCell>
                        <Table.HeaderCell>Coef </Table.HeaderCell>
                        <Table.HeaderCell>Enseignant </Table.HeaderCell>
                        <Table.HeaderCell>status</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <TableBody>
                    <Table.Row>
                        <Table.Cell>Anglais</Table.Cell>
                        <Table.Cell>2</Table.Cell>
                        <Table.Cell>Teucheu yves</Table.Cell>
                        <Table.Cell>
                            <div style={{ width: "100%", height: "100%", backgroundColor: "green" }}>
                                _
                            </div>
                        </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Anglais</Table.Cell>
                        <Table.Cell>2</Table.Cell>
                        <Table.Cell>Teucheu yves</Table.Cell>
                        <Table.Cell>
                            <div style={{ width: "100%", height: "100%", backgroundColor: "red" }}>
                                _
                            </div>
                        </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Anglais</Table.Cell>
                        <Table.Cell>2</Table.Cell>
                        <Table.Cell>Teucheu yves</Table.Cell>
                        <Table.Cell>
                            <div style={{ width: "100%", height: "100%", backgroundColor: "green" }}>
                                _
                            </div>
                        </Table.Cell>
                    </Table.Row>
                </TableBody>
            </Table>

        </Route>
    );
}

export default Matieres;