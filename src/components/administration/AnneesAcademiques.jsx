import React, { Component } from 'react';
import { Grid, GridRow, Table, Label, Menu, Icon, Button } from 'semantic-ui-react';

const AnneesAcademiques = ({ }) => {
    return (
        <Grid className="anneeAcademiqueList">
            <GridRow>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Nom Cycle</Table.HeaderCell>
                            <Table.HeaderCell>Section</Table.HeaderCell>
                            <Table.HeaderCell>Année Académique</Table.HeaderCell>
                            <Table.HeaderCell>Principal</Table.HeaderCell>
                            <Table.HeaderCell>Prefet détude</Table.HeaderCell>
                            <Table.HeaderCell>Surveillants</Table.HeaderCell>
                            <Table.HeaderCell>Editer</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>1 er cycle</Table.Cell>
                            <Table.Cell>Anglophone</Table.Cell>
                            <Table.Cell>2019-2020</Table.Cell>
                            <Table.Cell>M Mvodo</Table.Cell>
                            <Table.Cell>M. Paul</Table.Cell>
                            <Table.Cell>M. Jean</Table.Cell>
                            <Table.Cell><Button>Modifier</Button></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>1 er cycle</Table.Cell>
                            <Table.Cell>Anglophone</Table.Cell>
                            <Table.Cell>2019-2020</Table.Cell>
                            <Table.Cell>M Mvodo</Table.Cell>
                            <Table.Cell>M. Paul</Table.Cell>
                            <Table.Cell>M. Jean</Table.Cell>
                            <Table.Cell><Button>Modifier</Button></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>1 er cycle</Table.Cell>
                            <Table.Cell>Anglophone</Table.Cell>
                            <Table.Cell>2019-2020</Table.Cell>
                            <Table.Cell>M Mvodo</Table.Cell>
                            <Table.Cell>M. Paul</Table.Cell>
                            <Table.Cell>M. Jean</Table.Cell>
                            <Table.Cell><Button>Modifier</Button></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>1 er cycle</Table.Cell>
                            <Table.Cell>Anglophone</Table.Cell>
                            <Table.Cell>2019-2020</Table.Cell>
                            <Table.Cell>M Mvodo</Table.Cell>
                            <Table.Cell>M. Paul</Table.Cell>
                            <Table.Cell>M. Jean</Table.Cell>
                            <Table.Cell><Button>Modifier</Button></Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>1 er cycle</Table.Cell>
                            <Table.Cell>Anglophone</Table.Cell>
                            <Table.Cell>2019-2020</Table.Cell>
                            <Table.Cell>M Mvodo</Table.Cell>
                            <Table.Cell>M. Paul</Table.Cell>
                            <Table.Cell>M. Jean</Table.Cell>
                            <Table.Cell><Button>Modifier</Button></Table.Cell>
                        </Table.Row>
                    </Table.Body>

                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan='7'>
                                <Menu floated='right' pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron left' />
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a'>3</Menu.Item>
                                    <Menu.Item as='a'>4</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron right' />
                                    </Menu.Item>
                                </Menu>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
            </GridRow>
        </Grid>
    );
}

export default AnneesAcademiques;