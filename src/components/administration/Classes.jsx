import React, { Component } from 'react';
import { Grid, GridRow, Table, Label, Menu, Icon } from 'semantic-ui-react';

const Classes = ({ }) => {
    return (
        <Grid className="anneeAcademiqueList">
            <GridRow>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Classe</Table.HeaderCell>
                            <Table.HeaderCell>Pension</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>3eme A</Table.Cell>
                            <Table.Cell>55 000 FCFA</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>4ème B</Table.Cell>
                            <Table.Cell>40 000 FCFA</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>3eme A</Table.Cell>
                            <Table.Cell>55 000 FCFA</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>4ème B</Table.Cell>
                            <Table.Cell>40 000 FCFA</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>3eme A</Table.Cell>
                            <Table.Cell>55 000 FCFA</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>4ème B</Table.Cell>
                            <Table.Cell>40 000 FCFA</Table.Cell>
                        </Table.Row>
                    </Table.Body>

                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan='2'>
                                <Menu floated='right' pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron left' />
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron right' />
                                    </Menu.Item>
                                </Menu>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
            </GridRow>
        </Grid>
    );
}

export default Classes;