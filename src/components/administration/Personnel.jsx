import React, { Component } from 'react';
import { Grid, GridRow, Table, Label, Menu, Icon, Button } from 'semantic-ui-react';

const Personnel = ({ }) => {
    return (
        <Grid className="anneeAcademiqueList">
            <GridRow>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Nom</Table.HeaderCell>
                            <Table.HeaderCell>Grade</Table.HeaderCell>
                            <Table.HeaderCell>Statut</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>M. Mvondo Paul</Table.Cell>
                            <Table.Cell>Enseignant</Table.Cell>
                            <Table.Cell>Permanant</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>M. Mvondo Paul</Table.Cell>
                            <Table.Cell>Enseignant</Table.Cell>
                            <Table.Cell>Permanant</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>M. Mvondo Paul</Table.Cell>
                            <Table.Cell>Enseignant</Table.Cell>
                            <Table.Cell>Permanant</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>M. Mvondo Paul</Table.Cell>
                            <Table.Cell>Enseignant</Table.Cell>
                            <Table.Cell>Permanant</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>M. Mvondo Paul</Table.Cell>
                            <Table.Cell>Enseignant</Table.Cell>
                            <Table.Cell>Permanant</Table.Cell>
                        </Table.Row>
                    </Table.Body>

                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan='7'>
                                <Menu floated='right' pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron left' />
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a'>3</Menu.Item>
                                    <Menu.Item as='a'>4</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron right' />
                                    </Menu.Item>
                                </Menu>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
            </GridRow>
        </Grid>
    );
}

export default Personnel;