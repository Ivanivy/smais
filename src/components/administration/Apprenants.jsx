import React, { Component } from 'react';
import { Grid, GridRow, Table, Label, Menu, Icon, Button } from 'semantic-ui-react';

const Apprenants = ({ }) => {
    return (
        <Grid className="anneeAcademiqueList">
            <GridRow>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Nom </Table.HeaderCell>
                            <Table.HeaderCell>Classe</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>John Doe</Table.Cell>
                            <Table.Cell>3ème</Table.Cell>
                        
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Abena Marie</Table.Cell>
                            <Table.Cell>2nde</Table.Cell>
                            
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Dongmo Pierre</Table.Cell>
                            <Table.Cell>6ème</Table.Cell>
                            
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Tchanji junior</Table.Cell>
                            <Table.Cell>3ème</Table.Cell>
                            
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Aletanou martial</Table.Cell>
                            <Table.Cell>3ème</Table.Cell>
                            
                        </Table.Row>
                    </Table.Body>

                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan='7'>
                                <Menu floated='right' pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron left' />
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a'>3</Menu.Item>
                                    <Menu.Item as='a'>4</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron right' />
                                    </Menu.Item>
                                </Menu>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
            </GridRow>
        </Grid>
    );
}
export default Apprenants;