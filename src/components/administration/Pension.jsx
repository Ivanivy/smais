import React, { Component } from 'react';
import { Grid, GridRow, Table, Label, Menu, Icon, Button } from 'semantic-ui-react';

const Pension = ({ }) => {
    return (
        <Grid className="anneeAcademiqueList">
            <GridRow>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Classe</Table.HeaderCell>
                            <Table.HeaderCell>Inscription</Table.HeaderCell>
                            <Table.HeaderCell>1ère tranche</Table.HeaderCell>
                            <Table.HeaderCell>2ème tranche</Table.HeaderCell>
                            <Table.HeaderCell>Total</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>2nde</Table.Cell>
                            <Table.Cell>10,000 FCFA</Table.Cell>
                            <Table.Cell>25,000</Table.Cell>
                            <Table.Cell>30,000</Table.Cell>
                            <Table.Cell>65,000</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>2nde</Table.Cell>
                            <Table.Cell>10,000 FCFA</Table.Cell>
                            <Table.Cell>25,000</Table.Cell>
                            <Table.Cell>30,000</Table.Cell>
                            <Table.Cell>65,000</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>2nde</Table.Cell>
                            <Table.Cell>10,000 FCFA</Table.Cell>
                            <Table.Cell>25,000</Table.Cell>
                            <Table.Cell>30,000</Table.Cell>
                            <Table.Cell>65,000</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>2nde</Table.Cell>
                            <Table.Cell>10,000 FCFA</Table.Cell>
                            <Table.Cell>25,000</Table.Cell>
                            <Table.Cell>30,000</Table.Cell>
                            <Table.Cell>65,000</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>2nde</Table.Cell>
                            <Table.Cell>10,000 FCFA</Table.Cell>
                            <Table.Cell>25,000</Table.Cell>
                            <Table.Cell>30,000</Table.Cell>
                            <Table.Cell>65,000</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>2nde</Table.Cell>
                            <Table.Cell>10,000 FCFA</Table.Cell>
                            <Table.Cell>25,000</Table.Cell>
                            <Table.Cell>30,000</Table.Cell>
                            <Table.Cell>65,000</Table.Cell>

                        </Table.Row>
                    </Table.Body>

                    <Table.Footer>
                        <Table.Row>
                            <Table.HeaderCell colSpan='5'>
                                <Menu floated='right' pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron left' />
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a'>3</Menu.Item>
                                    <Menu.Item as='a'>4</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='chevron right' />
                                    </Menu.Item>
                                </Menu>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
            </GridRow>
        </Grid>
    );
}

export default Pension;