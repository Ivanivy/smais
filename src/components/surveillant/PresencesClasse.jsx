import React, { Component } from 'react'
import { Grid, GridRow, Form, Select, Search, Table, TableRow, TableHeader, TableCell, TableBody, Modal, Button, Input } from 'semantic-ui-react';



const PresencesClasse = ({ styles }) => {

    const principalOptions = [
        { key: '1', value: "3ème", text: "3ème" },
        { key: '2', value: "4ème", text: "4ème" },
        { key: '3', value: "5ème", text: "5ème" },
    ]

    return (
        <Grid>
            <GridRow>
                <div style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center", marginTop: "1rem" }}>
                    <Search placeholder="rechercher" />
                </div>
            </GridRow>
            <GridRow>
                <Form>
                    <Form.Field inline>
                        <label>Classe: </label>
                        <Select placeholder='Choisir la classe' options={principalOptions} />
                    </Form.Field>
                </Form>
            </GridRow>
            <GridRow>
                <Table>
                    <TableHeader>
                        <TableRow>
                            <TableCell>
                                <h3>
                                    Nombre d'heures
                                </h3>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                <strong>Noms et prénoms</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Seq 1</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Seq 2</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Seq 3</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Seq 4</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Seq 5</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Seq 6</strong>
                            </TableCell>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                ALIOU ALIBA
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                <div className={styles.ajouterHeureButton}>

                                </div>
                            </TableCell>
                            <TableCell>
                                <div className={styles.ajouterHeureButtonOutlined}>

                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                ALIOU ALIBA
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                <div className={styles.ajouterHeureButton}>

                                </div>
                            </TableCell>
                            <TableCell>
                                <div className={styles.ajouterHeureButtonOutlined}>

                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                ALIOU ALIBA
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                <div className={styles.ajouterHeureButton}>

                                </div>
                            </TableCell>
                            <TableCell>
                                <div className={styles.ajouterHeureButtonOutlined}>

                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                ALIOU ALIBA
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                <div className={styles.ajouterHeureButton}>

                                </div>
                            </TableCell>
                            <TableCell>
                                <div className={styles.ajouterHeureButtonOutlined}>

                                </div>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                <Modal open={false}>
                    <Modal.Header>
                        <h2>Insérer le nombre d'heures</h2>
                    </Modal.Header>
                    <Form>
                        <div style={{ marginTop: "1rem", marginBottom: "1rem" }}>
                            <Form.Field inline>
                                <label>Séquence: </label>
                                <Select placeholder="choisir la séquence" options={principalOptions} />
                            </Form.Field>

                            <Form.Field inline>
                                <label>Nombre d'heures: </label>
                                <Select placeholder="choisir le nombre d'heures" options={principalOptions} />
                            </Form.Field>
                        </div>
                    </Form>
                    <Modal.Actions>
                        <Button>Ok</Button>
                    </Modal.Actions>
                </Modal>
            </GridRow>
        </Grid>
    )
}

export default PresencesClasse;