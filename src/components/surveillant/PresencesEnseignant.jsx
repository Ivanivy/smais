import React, { Component } from 'react'
import { Grid, GridRow, Form, Select, Search, Table, TableRow, TableHeader, TableCell, TableBody, Modal, Button, Input, ModalActions, ModalHeader } from 'semantic-ui-react';

const PresencesEnseignant = ({ styles }) => {

    const principalOptions = [
        { key: '1', value: "3ème", text: "3ème" },
        { key: '2', value: "4ème", text: "4ème" },
        { key: '3', value: "5ème", text: "5ème" },
    ]

    return (
        <Grid>
            <GridRow>
                <div style={{ width: "100%", display: "flex", justifyContent: "center", alignItems: "center", marginTop: "1rem" }}>
                    <Search placeholder="rechercher" />
                </div>
            </GridRow>
            <GridRow>
                <h3>Date: 18-03-2020</h3>
            </GridRow>
            <GridRow>
                <Modal trigger={<Button>+ Ajouter Emargement</Button>} >
                    <Form>
                        <div style={{ marginTop: "1rem", marginBottom: "1rem", display: "flex", flexDirection: "row" }}>
                            <Form.Field inline>
                                <label>Séquence: </label>
                                <Select placeholder="choisir la séquence" options={principalOptions} />
                            </Form.Field>
                            <div style={{ margin: "1rem" }}></div>
                            <Form.Field inline>
                                <label>Nombre d'heures: </label>
                                <Select placeholder="choisir le nombre d'heures" options={principalOptions} />
                            </Form.Field>
                        </div>
                    </Form>
                    <ModalActions>
                        <Button>Ok</Button>
                    </ModalActions>
                </Modal>
            </GridRow>
            <GridRow>
                <Table>
                    <TableHeader>
                        <TableRow>
                            <TableCell>
                                <strong>Date</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Nom prof</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Classes</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Heure Arrivée</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Heure Départ</strong>
                            </TableCell>
                            <TableCell>
                                <strong>Total</strong>
                            </TableCell>
                            <TableCell colSpan={2}>
                                <strong>Statuts</strong>
                            </TableCell>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                ALIOU ALIBA
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.ajouterHeureButton} ${styles.enseignantAction} ${styles.enseignantAction} `}>

                                </div>
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.ajouterHeureButtonOutlined}`}>

                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                ALIOU ALIBA
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.ajouterHeureButton} ${styles.enseignantAction} ${styles.enseignantAction} `}>

                                </div>
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.ajouterHeureButtonOutlined}`}>

                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                ALIOU ALIBA
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.ajouterHeureButton} ${styles.enseignantStatusDanger} ${styles.enseignantAction} ${styles.enseignantAction} `}>

                                </div>
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.ajouterHeureButtonOutlined}`}>

                                </div>
                            </TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>
                                ALIOU ALIBA
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                2h
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.ajouterHeureButton} ${styles.enseignantAction} ${styles.enseignantAction} `}>

                                </div>
                            </TableCell>
                            <TableCell>
                                <div className={`${styles.ajouterHeureButtonOutlined}`}>

                                </div>
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
                <Modal open={false}>
                    <Modal.Header>
                        <h2>Insérer le nombre d'heures</h2>
                    </Modal.Header>
                    <Form>
                        <div style={{ marginTop: "1rem", marginBottom: "1rem" }}>
                            <Form.Field inline>
                                <label>Séquence: </label>
                                <Select placeholder="choisir la séquence" options={principalOptions} />
                            </Form.Field>

                            <Form.Field inline>
                                <label>Nombre d'heures: </label>
                                <Select placeholder="choisir le nombre d'heures" options={principalOptions} />
                            </Form.Field>
                        </div>
                    </Form>
                    <Modal.Actions>
                        <Button>Ok</Button>
                    </Modal.Actions>
                </Modal>
            </GridRow>
            <Modal open={false}>
                <ModalHeader>
                    Heure Départ
                </ModalHeader>
                <div style={{width:"100%",display:"flex",alignItems:"center",justifyContent:"center"}}>
                    <h1>13 : 45 </h1>
                </div>
                <ModalActions>
                    <Button>Ok</Button>
                </ModalActions>
            </Modal>
        </Grid>
    )
}

export default PresencesEnseignant;