import React from 'react';
import { Grid, GridColumn} from 'semantic-ui-react';
import {withRouter} from 'react-router-dom';

const DashboardCard = ({title,icon,amount,targetedPath,history}) => {
    const onClickListener = (path)=>{
        console.log(path);
        history.push(path);
    }
    return (
        <GridColumn>
            <div className="dashboard-card " onClick={()=>onClickListener(targetedPath)}>
                <Grid columns="equal">
                    <GridColumn verticalAlign="bottom" className="dashboard-card-label">
                        {title}
                    </GridColumn>
                    <GridColumn width={8}>
                        <div>
                            <div className="dashboard-card-icon">
                                {icon}
                            </div>
                            <div className="amount">
                                {amount}
                            </div>
                        </div>
                    </GridColumn>
                </Grid>
            </div>
        </GridColumn>
    );
};

export default withRouter(DashboardCard);