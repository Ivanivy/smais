import React, { Component } from 'react'
import { Grid, GridColumn, Form, Button, Select, Modal, List, Image, Checkbox, Search } from 'semantic-ui-react';
import { Route, Link, withRouter } from 'react-router-dom';
import Econome from '../../pages/Econome';

const AjouterApprenant = ({ styles, history }) => {

    const principalOptions = [
        { key: '1', value: "3ème", text: "3ème" },
        { key: '2', value: "4ème", text: "4ème" },
        { key: '3', value: "5ème", text: "5ème" },
    ]

    return (
        <Grid stretched>
            <GridColumn>
                <div>
                    <Route path="/econome">
                        <h1>Inscrire un apprenant  </h1>
                    </Route>

                    <div>
                        <Form className={`${styles.addEtudiantForm}`}>
                            <Form.Field inline>
                                <label>Apprenant: </label>
                                <Modal
                                    trigger={<Link to="/econome/inscription/addapprenant"><Button>Choisir</Button></Link>}
                                    onClose={() => { history.push("/econome/inscription") }}
                                >
                                    <Modal.Header>
                                        <Route path="/econome/inscription/addapprenant" exact>
                                            Ajouter un apprennant
                                        </Route>
                                        <Route path="/econome/inscription/addparent" exact>
                                            Selectionner Parent
                                        </Route>
                                        <div className={styles.searchContainer} exact>
                                            <div style={{ display: "flex" }}>
                                                <Search />
                                                <Route path="/econome/inscription/addparent" exact>
                                                    <div style={{ marginLeft: "auto" }}>
                                                        <Modal trigger={<Button>Ajouter Parent</Button>}>
                                                            <Modal.Header>Informations du parent</Modal.Header>
                                                            <div style={{ margin: "1rem" }}>
                                                                <Form>
                                                                    <Form.Field >
                                                                        <label>Nom:</label>
                                                                        <input placeholder="Entre le nom" />
                                                                    </Form.Field>
                                                                    <Form.Field >
                                                                        <label>Prenom:</label>
                                                                        <input placeholder="Entre le prénom" />
                                                                    </Form.Field>
                                                                    <Form.Field >
                                                                        <label>Email:</label>
                                                                        <input placeholder="Entre l'email" />
                                                                    </Form.Field>
                                                                    <Form.Field >
                                                                        <label>Tel:</label>
                                                                        <input placeholder="Entre le tél" />
                                                                    </Form.Field>
                                                                </Form>
                                                            </div>
                                                            <Modal.Actions>
                                                                <Button>Ok</Button>
                                                            </Modal.Actions>
                                                        </Modal>
                                                    </div>
                                                </Route>
                                            </div>
                                        </div>
                                    </Modal.Header>
                                    <Route path="/econome/inscription/addapprenant">
                                        <List celled>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Fotso Noumsi Borice' /></strong>
                                                </List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Tchoukeu Nyassi Loic' /></strong>
                                                </List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Anama Adamou' /></strong>
                                                </List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Fredéric Jean' /></strong>
                                                </List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Fotso Noumsi Borice' /></strong>
                                                </List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Tchoukeu Nyassi Loic' /></strong>
                                                </List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Anama Adamou' /></strong>
                                                </List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Fredéric Jean' /></strong>
                                                </List.Content>
                                            </List.Item>
                                        </List>
                                    </Route>
                                    <Route path="/econome/inscription/addparent">
                                        <List celled>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Fotso Noumsi Borice' /></strong>
                                                </List.Content>
                                            </List.Item>
                                            <List.Item>
                                                <List.Content verticalAlign="middle">
                                                    <strong> <Checkbox label='Tchoukeu Nyassi Loic' /></strong>
                                                </List.Content>
                                            </List.Item>
                                        </List>
                                    </Route>
                                    <Modal.Actions>
                                        <Route path="/econome/inscription/addapprenant">
                                            <Button><Link to="/econome/inscription/addparent">Suivant</Link> </Button>
                                        </Route>
                                        <Route path="/econome/inscription/addparent">
                                            <Button>Ok</Button>
                                        </Route>
                                    </Modal.Actions>
                                </Modal>
                            </Form.Field>
                            <Form.Field inline>
                                <label>Classe: </label>
                                <Select placeholder='Choisir la classe' options={principalOptions} />
                            </Form.Field>
                        </Form>

                    </div>
                </div>
            </GridColumn>
        </Grid>
    );

}

export default withRouter(AjouterApprenant);