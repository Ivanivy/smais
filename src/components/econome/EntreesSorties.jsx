import React, { Component } from 'react'
import { GridColumn, GridRow, Search, Grid, Table, TableBody, Modal, Button, Form, PlaceholderImage, Image, ModalHeader, Select, Radio } from 'semantic-ui-react';
import { Route, Link } from 'react-router-dom';
import logo from '../../assets/icons/administration/logo.png';
class EntreesSorties extends Component {

    render() {
        const principalOptions = [
            { key: '1', value: "3ème", text: "3ème" },
            { key: '2', value: "4ème", text: "4ème" },
            { key: '3', value: "5ème", text: "5ème" },
        ]

        return (
            <GridColumn>
                <Grid>
                    <GridRow>
                        <Route path="/econome/entreessorties">
                            <div>
                                <div>
                                    <h1>Entrées Sorties</h1>
                                </div>
                            </div>
                        </Route>
                    </GridRow>
                    <GridRow>
                        <div style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                            <Search />
                            <Modal
                                trigger={<Button>+ Ajouter</Button>}
                            >
                                <ModalHeader>
                                    <h1 style={{ textAlign: "center" }}>Transaction</h1>
                                </ModalHeader>
                                <Form>
                                    <Form.Field >
                                        <label>Type utilisation:</label>
                                        <Select placeholder='Type de transaction' options={principalOptions} />
                                    </Form.Field>
                                    <Form.Field >
                                        <label>Motif transaction:</label>
                                        <input placeholder="Motif transaction" />
                                    </Form.Field>
                                    <Form.Field >
                                        <label>Type de transaction:</label>
                                        <Select placeholder='Type de transaction' options={principalOptions} />
                                    </Form.Field>
                                    <Form.Field >
                                        <label>Bénéficiaire:</label>
                                        <input placeholder="Bénéficiaire" />
                                    </Form.Field>
                                    <Form.Field >
                                        <label>Montant:</label>
                                        <input placeholder="Montant" />
                                    </Form.Field>
                                    <Form.Field >
                                        <label>Initiateur:</label>
                                        <input placeholder="Iinitiateur" />
                                    </Form.Field>
                                </Form>
                                <Modal.Actions>
                                    <Button>Ok</Button>
                                </Modal.Actions>
                            </Modal>

                        </div>
                        <div style={{ marginTop: "1.5rem", marginLeft: "auto" }}>
                            <Radio label={"All"} />
                            <Radio label={"Dépot"} />
                            <Radio label={"Dépensé"} />
                        </div>
                    </GridRow>
                    <GridRow>
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell width="8">Dates </Table.HeaderCell>
                                    <Table.HeaderCell>Motif</Table.HeaderCell>
                                    <Table.HeaderCell>Montant</Table.HeaderCell>
                                    <Table.HeaderCell>Type</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <TableBody>
                                <Table.Row>
                                    <Table.Cell ><Link>14-10-2021</Link></Table.Cell>
                                    <Table.Cell ><Link>versement</Link></Table.Cell>
                                    <Table.Cell ><Link>10 000</Link></Table.Cell>
                                    <Table.Cell ><Link>dépot</Link></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell ><Link>14-10-2021</Link></Table.Cell>
                                    <Table.Cell ><Link>versement</Link></Table.Cell>
                                    <Table.Cell ><Link>10 000</Link></Table.Cell>
                                    <Table.Cell ><Link>dépot</Link></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell ><Link>14-10-2021</Link></Table.Cell>
                                    <Table.Cell ><Link>versement</Link></Table.Cell>
                                    <Table.Cell ><Link>10 000</Link></Table.Cell>
                                    <Table.Cell ><Link>dépot</Link></Table.Cell>
                                </Table.Row>
                            </TableBody>
                        </Table>

                    </GridRow>
                </Grid>
            </GridColumn>
        );
    }
}


export default EntreesSorties;