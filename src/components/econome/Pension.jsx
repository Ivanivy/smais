import React, { Component } from 'react'
import { GridColumn, GridRow, Search, Grid, Table, TableBody, Modal, Button, Form } from 'semantic-ui-react';
import { Route, Link } from 'react-router-dom';

class Pension extends Component {

    state = {
        open: false,
        students: [
            { id: 1, }
        ],
        selectedStudent: {

        }
    }

    render() {

        return (
            <GridColumn stretched className={this.props.styles.pensionContainer}>
                <Grid>
                    <GridRow>
                        <Route path="/econome/pension">
                            <h1>Pension</h1>
                        </Route>
                    </GridRow>
                    <GridRow>
                        <Search />
                    </GridRow>
                    <GridRow>
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell width="8">Nom </Table.HeaderCell>
                                    <Table.HeaderCell>1ère tranche </Table.HeaderCell>
                                    <Table.HeaderCell>2ème tranche </Table.HeaderCell>
                                    <Table.HeaderCell>3ème tranche </Table.HeaderCell>
                                    <Table.HeaderCell>status</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <TableBody>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link onClick={() => { this.setState({ open: true }) }}>FOTSO NOUMSI NYAMSSI</Link></Table.Cell>
                                    <Table.Cell>30 000</Table.Cell>
                                    <Table.Cell>30 000</Table.Cell>
                                    <Table.Cell>15 000</Table.Cell>
                                    <Table.Cell >
                                        <div style={{ width: "100%", height: "100%", backgroundColor: "green" }}>
                                        </div>
                                    </Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link onClick={() => { this.setState({ open: true }) }}>FOTSO NOUMSI NYAMSSI</Link></Table.Cell>
                                    <Table.Cell>30 000</Table.Cell>
                                    <Table.Cell>30 000</Table.Cell>
                                    <Table.Cell>15 000</Table.Cell>
                                    <Table.Cell >
                                        <div style={{ width: "100%", height: "100%", backgroundColor: "green" }}>
                                        </div>
                                    </Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link onClick={() => { this.setState({ open: true }) }}>FOTSO NOUMSI NYAMSSI</Link></Table.Cell>
                                    <Table.Cell>30 000</Table.Cell>
                                    <Table.Cell>30 000</Table.Cell>
                                    <Table.Cell>15 000</Table.Cell>
                                    <Table.Cell >
                                        <div style={{ width: "100%", height: "100%", backgroundColor: "red" }}>
                                        </div>
                                    </Table.Cell>
                                </Table.Row>
                                <Modal open={this.state.open} closeOnDimmerClick={"true"} closeOnDocumentClick={"true"}>
                                    <Modal.Header>
                                        <div className={this.props.styles.pensionHeader}>
                                            <div>
                                                <div>Nom: FOTSO NOUMSI</div>
                                                <div>Classe: 3ème</div>
                                            </div>
                                            <div>
                                                <div>Prenom: Yvan</div>
                                                <div>Age:15</div>
                                            </div>

                                            <div>
                                                <div>
                                                    <Modal

                                                        trigger={<Link to="/econome/pension/addsomme"><Button>+ Nouveau Versement</Button></Link>}
                                                    >
                                                        <Route path="/econome/pension/addsomme" exact>
                                                            <h1 style={{ textAlign: "center", marginTop: "1rem", marginBottom: "1rem" }}>Somme versée</h1>
                                                            <Form>
                                                                <Form.Field >
                                                                    <input placeholder="Montant en FCFA" />
                                                                </Form.Field>
                                                            </Form>
                                                        </Route>
                                                        <Route path="/econome/pension/addsomme/confirm">
                                                            <Form>
                                                                <h1 style={{ textAlign: "center" }}>Confirmer</h1>
                                                            </Form>
                                                        </Route>


                                                        <Modal.Actions>
                                                            <Route path="/econome/pension/addsomme" exact>
                                                                <Link to="/econome/pension/addsomme/confirm"><Button>Enregistrer</Button></Link>
                                                            </Route>
                                                            <Route path="/econome/pension/addsomme/confirm">
                                                                <Button color="red">Non</Button>
                                                                <Button>Oui</Button>
                                                            </Route>
                                                        </Modal.Actions>
                                                    </Modal>
                                                </div>
                                            </div>
                                        </div>
                                    </Modal.Header>
                                    <div>
                                        <Table celled>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell>Motif</Table.HeaderCell>
                                                    <Table.HeaderCell>Date</Table.HeaderCell>
                                                    <Table.HeaderCell>Prix</Table.HeaderCell>
                                                    <Table.HeaderCell>Auteur</Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                            </Table.Body>
                                        </Table>
                                    </div>
                                </Modal>
                            </TableBody>
                        </Table>

                    </GridRow>
                </Grid>
            </GridColumn>
        );
    }

}


export default Pension;