import React, { Component } from 'react'
import { Line } from 'react-chartjs';
import { GridColumn } from 'semantic-ui-react';

class EtatsFinanciers extends Component {
    state = {
    };

    render() {
        var data = {
            labels: ["2 jours", "20 jours", "100 jours", "120 jours", "130 jours", "140 jours", "200 jours"],
            datasets: [
                {
                    label: "Dépenses",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [0, 4000, 7000, 5000, 1000, 10000, 4000]
                },
                {
                    label: "Entrées sorties",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [0, 1000, 3000, 2000, 5000, 1500, 650]
                }, {
                    label: "Montant en caisse",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(0,120,212,1)",
                    pointColor: "rgba(0,120,212,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [0, 1300, 2000, 2500, 1000, 3500, 7000]
                }
            ]
        };
        var config = {

            ///Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,

            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",

            //Number - Width of the grid lines
            scaleGridLineWidth: 1,

            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,

            //Boolean - Whether the line is curved between points
            bezierCurve: true,

            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.4,

            //Boolean - Whether to show a dot for each point
            pointDot: true,

            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,

            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,

            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,

            //Boolean - Whether to fill the dataset with a colour

            //Boolean - Whether to horizontally center the label and point dot inside the grid
            offsetGridLines: false, responsive: true,
            maintainAspectRatio: true,
            legend:{
                display:true,
            }
        };
        return (
            <GridColumn>
                <Line data={data} options={config} />
            </GridColumn>
        );
    }
}

export default EtatsFinanciers;