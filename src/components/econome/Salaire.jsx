import React, { Component } from 'react'
import { GridColumn, GridRow, Search, Grid, Table, TableBody, Modal, Button, Form, PlaceholderImage, Image, ModalHeader, Select, Radio,Menu,Icon } from 'semantic-ui-react';
import { Route, Link } from 'react-router-dom';
import logo from '../../assets/icons/administration/logo.png';
class salaire extends Component {

    render() {
        const principalOptions = [
            { key: '1', value: "3ème", text: "3ème" },
            { key: '2', value: "4ème", text: "4ème" },
            { key: '3', value: "5ème", text: "5ème" },
        ]

        return (
            <GridColumn>
                <Grid>
                    <GridRow>
                        <Route path="/econome/salaire">
                            <div>
                                <div>
                                    <h1>Entrées Sorties</h1>
                                </div>
                            </div>
                        </Route>
                    </GridRow>
                    <GridRow>
                        <div style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                            <Search />
                        </div>
                        <div style={{ marginTop: "1.5rem", marginLeft: "auto" }}>
                            <Radio label={"Permanant"} />
                            <Radio label={"Non permanant"} />
                        </div>
                    </GridRow>
                    <GridRow>
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell width="8">Nom enseignant </Table.HeaderCell>
                                    <Table.HeaderCell>Prix</Table.HeaderCell>
                                    <Table.HeaderCell>Status</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <TableBody>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>Dr Kamdem</Link></Table.Cell>
                                    <Table.Cell width="10" ><Link>150 000</Link></Table.Cell>
                                    <Table.Cell ><div style={{ width: "100%", height: "100%", backgroundColor: "green" }}>
                                    </div></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>M. Robert</Link></Table.Cell>
                                    <Table.Cell width="10" ><Link>150 000</Link></Table.Cell>
                                    <Table.Cell ><div style={{ width: "100%", height: "100%", backgroundColor: "red" }}>
                                    </div></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>M. Lakoune</Link></Table.Cell>
                                    <Table.Cell width="10" ><Link>150 000</Link></Table.Cell>
                                    <Table.Cell ><div style={{ width: "100%", height: "100%", backgroundColor: "red" }}>
                                    </div></Table.Cell>
                                </Table.Row>
                            </TableBody>
                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='7'>
                                        <Menu floated='right' pagination>
                                            <Menu.Item as='a' icon>
                                                <Icon name='chevron left' />
                                            </Menu.Item>
                                            <Menu.Item as='a'>1</Menu.Item>
                                            <Menu.Item as='a'>2</Menu.Item>
                                            <Menu.Item as='a'>3</Menu.Item>
                                            <Menu.Item as='a'>4</Menu.Item>
                                            <Menu.Item as='a' icon>
                                                <Icon name='chevron right' />
                                            </Menu.Item>
                                        </Menu>
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>
                    </GridRow>
                </Grid>
            </GridColumn>
        );
    }
}


export default salaire;