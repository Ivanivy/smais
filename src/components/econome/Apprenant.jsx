import React, { Component } from 'react'
import { GridColumn, GridRow, Search, Grid, Table, TableBody, Modal, Button, Form, PlaceholderImage, Image } from 'semantic-ui-react';
import { Route, Link } from 'react-router-dom';
import logo from '../../assets/icons/administration/logo.png';
class Apprenant extends Component {
    render() {
        return (
            <GridColumn>
                <Grid>
                    <GridRow>
                        <Route path="/econome/pension">
                            <h1>Apprenants</h1>
                        </Route>
                    </GridRow>
                    <GridRow>
                        <div style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                            <Search />
                            <Modal
                                trigger={<Button>+ Ajouter</Button>}
                            >
                                <Form>
                                    <Form.Field >
                                        <label>Nom:</label>
                                        <input placeholder="Entre le nom" />
                                    </Form.Field>
                                    <Form.Field >
                                        <label>Prenom:</label>
                                        <input placeholder="Entre le prénom" />
                                    </Form.Field>
                                    <Form.Field >
                                        <label>Date Naissance:</label>
                                        <input placeholder="Entrez la date de naissance" />
                                    </Form.Field>
                                    <Form.Field >
                                        <label>photo:</label>
                                        <PlaceholderImage square>
                                            <img src={logo} alt="" srcset="" />
                                        </PlaceholderImage>
                                    </Form.Field>
                                </Form>
                                <Modal.Actions>
                                    <Button>Enregistrer</Button>
                                </Modal.Actions>
                            </Modal>

                        </div>
                    </GridRow>
                    <GridRow>
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell width="8">Nom </Table.HeaderCell>
                                    <Table.HeaderCell>Modifier</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <TableBody>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>FOTSO NOUMSI NYAMSSI</Link></Table.Cell>
                                    <Table.Cell><Button>Modifier</Button></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>FOTSO NOUMSI NYAMSSI</Link></Table.Cell>
                                    <Table.Cell><Button>Modifier</Button></Table.Cell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.Cell textAlign="left"><Link>FOTSO NOUMSI NYAMSSI</Link></Table.Cell>
                                    <Table.Cell><Button>Modifier</Button></Table.Cell>
                                </Table.Row>
                                <Modal>
                                    <Modal.Header>
                                        <div className={this.props.styles.pensionHeader}>
                                            <div>
                                                <div>Nom: FOTSO NOUMSI</div>
                                                <div>Classe: 3ème</div>
                                            </div>
                                            <div>
                                                <div>Prenom: Yvan</div>
                                                <div>Age:15</div>
                                            </div>

                                            <div>
                                                <div>
                                                    dfsfsd
                                                </div>
                                            </div>
                                        </div>
                                    </Modal.Header>
                                    <div>
                                        <Table celled>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell>Motif</Table.HeaderCell>
                                                    <Table.HeaderCell>Date</Table.HeaderCell>
                                                    <Table.HeaderCell>Prix</Table.HeaderCell>
                                                    <Table.HeaderCell>Auteur</Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>1ère tranche</Table.Cell>
                                                    <Table.Cell>14-03-2020</Table.Cell>
                                                    <Table.Cell>35 000</Table.Cell>
                                                    <Table.Cell>M. Paul</Table.Cell>
                                                </Table.Row>
                                            </Table.Body>
                                        </Table>
                                    </div>
                                </Modal>
                            </TableBody>
                        </Table>

                    </GridRow>
                </Grid>
            </GridColumn>
        );
    }
}


export default Apprenant;